import os

from jinja2 import Environment, FileSystemLoader


env_data = dict(os.environ)

def render_config():
    jinja_env = Environment(loader=FileSystemLoader('.'))
    template = jinja_env.get_template(env_data["TMPL_CONF"])
    data = template.render(**env_data)
    open(env_data["DEFAULT_CONF"], "w").write(data)


if __name__ == '__main__':
    render_config()